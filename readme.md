Preparar o chatwoot

`docker compose run --rm chatwoot bundle exec rails db:chatwoot_prepare`

Subir o chatwoot

`docker compose up`


Login com 

```
url: http://localhost:3000
user_name: john@acme.inc
password: Password1!
```

Pegar o valor do token como na imagem e colocar na env POSTBACK_SERVER_AUTH_TOKEN


![image](copy_token.png)


reiniciar todo os serviços

`docker compose down` e `docker compose up`


Criar o canal, o token de autenticacao é o conteudo da env `WHATSAPP_API_SERVER_AUTH_TOKEN` a url `http://localhost:9999/cloud-api` e o restante preenche com o numero de whatsapp que vai conectar

![image](create_channel.png)

Criar um contato com o mesmo numero do whatsappp e mande um mensage

![image](create_contact.png)

Leia o qrcode e seja feliz! =)

![image](ler_qrcode.png)


Para deletar todos os dados e começar um ambiente novo

`docker volume remove chatwoot-baileys_redis chatwoot-baileys_postgres chatwoot-baileys_storage `